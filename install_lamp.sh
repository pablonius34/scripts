#!/bin/bash

# Colores
rojo='\e[0;31m'
verde='\e[0;32m'
amarillo='\e[0;33m'
reset='\e[0m'  # Resetea los colores

lampinstall(){
    echo -e "${verde}Update repositorys of linux${reset}" 
    sleep 3
    sudo apt update
    echo -e "${verde}Install apache server....${reset}"
    sleep 3
    sudo apt install apache2 apache2-utils
    echo -e "${verde}List ufw....${reset}"
    sudo ufw app list
    sudo ufw app info "Apache Full"
    sudo ufw allow "Apache Full"
    sudo apt install curl
    curl http://icanhazip.com
    echo -e "${verde}Install mysql database...${reset}"
    sleep 3    
    sudo apt install mysql-server
    echo -e "${verde}Install php....${reset}"
    sleep 3
    sudo apt install php libapache2-mod-php php-mysql
}

config_apache(){
    echo ""
}

config_mysql(){
    echo ""
}

show_info(){
    echo -e "${verde} IP DEL SERVIDOR${reset}"
    ipaddress=$(hostname -I)
    echo $ipaddress
    echo -e "${amarillo}http://$ipaddress"
}

echo "LAMP LINUX + APACHE + MYSQL + PHP"
lampinstall
show_info
